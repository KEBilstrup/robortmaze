﻿using UnityEngine;
using System.Collections;

//<summary>
//Ball movement controlls and simple third-person-style camera
//</summary>
public class RollerBall : MonoBehaviour {

	//public GameObject ViewCamera = null;
	public AudioClip JumpSound = null;
	public AudioClip HitSound = null;
	public AudioClip CoinSound = null;
    public float SWIPE_THRESHOLD = 100f;

    private Rigidbody mRigidBody = null;
	private AudioSource mAudioSource = null;
	private bool mFloorTouched = false;

    void Start()
    {
        mRigidBody = GetComponent<Rigidbody>();
        mAudioSource = GetComponent<AudioSource>();
        if (mRigidBody != null)
        {
            //mRigidBody.velocity = transform.forward * 1;
        }
    }

	void FixedUpdate () {
        if (mRigidBody != null)
        {            
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                // Get movement of the finger since last frame
                Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
                // Move object across XY plane
                if (touchDeltaPosition.x > SWIPE_THRESHOLD)
                {
                    AndroidNativeFunctions.ShowToast("RightSwipe", true);
                    //mRigidBody.position += Vector3.right * Time.deltaTime * 1;
                    //mRigidBody.velocity = transform.right * 1;
                    //transform.Rotate(0, 90, 0);
                }
                else if (touchDeltaPosition.x < -SWIPE_THRESHOLD)
                {
                    AndroidNativeFunctions.ShowToast("LeftSwipe", true);
                }
            }
            //if (ViewCamera != null)
            //{
            //    Vector3 direction = (Vector3.up * 2 + Vector3.back) * 2;
            //    RaycastHit hit;
            //    Debug.DrawLine(transform.position, transform.position + direction, Color.red);
            //    if (Physics.Linecast(transform.position, transform.position + direction, out hit))
            //    {
            //        ViewCamera.transform.position = hit.point;
            //    }
            //    else
            //    {
            //        ViewCamera.transform.position = transform.position + direction;
            //    }
            //    ViewCamera.transform.LookAt(transform.position);
            //}
        }
	}

	void OnCollisionEnter(Collision coll){
		if (coll.gameObject.tag.Equals ("Floor")) {
			mFloorTouched = true;
			if (mAudioSource != null && HitSound != null && coll.relativeVelocity.y > .5f) {
				mAudioSource.PlayOneShot (HitSound, coll.relativeVelocity.magnitude);
			}
		} else {
			if (mAudioSource != null && HitSound != null && coll.relativeVelocity.magnitude > 2f) {
				mAudioSource.PlayOneShot (HitSound, coll.relativeVelocity.magnitude);
			}
		}
	}

	void OnCollisionExit(Collision coll){
		if (coll.gameObject.tag.Equals ("Floor")) {
			mFloorTouched = false;
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag.Equals ("Coin")) {
			if(mAudioSource != null && CoinSound != null){
				mAudioSource.PlayOneShot(CoinSound);
			}
			Destroy(other.gameObject);
		}
	}
}
