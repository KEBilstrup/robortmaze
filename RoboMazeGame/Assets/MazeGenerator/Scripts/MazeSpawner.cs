﻿using UnityEngine;
using System.Collections;

//<summary>
//Game object, that creates maze and instantiates it in scene
//</summary>
public class MazeSpawner : MonoBehaviour {
	public enum MazeGenerationAlgorithm{
		PureRecursive,
		RecursiveTree,
		RandomTree,
		OldestTree,
		RecursiveDivision,
	}

	public MazeGenerationAlgorithm Algorithm = MazeGenerationAlgorithm.PureRecursive;
	public bool FullRandom = false;
	public int RandomSeed = 12345;
	public GameObject Floor = null;
	public GameObject Wall = null;
	public GameObject Pillar = null;
	public int Rows = 5;
	public int Columns = 5;
	public float CellWidth = 5;
	public float CellHeight = 5;
	public bool AddGaps = true;
	public GameObject Kryptonite = null;

	private BasicMazeGenerator mMazeGenerator = null;

	void Start () {
		if (!FullRandom) {
			Random.seed = RandomSeed;
		}
		switch (Algorithm) {
		case MazeGenerationAlgorithm.PureRecursive:
			mMazeGenerator = new RecursiveMazeGenerator (Rows, Columns);
			break;
		case MazeGenerationAlgorithm.RecursiveTree:
			mMazeGenerator = new RecursiveTreeMazeGenerator (Rows, Columns);
			break;
		case MazeGenerationAlgorithm.RandomTree:
			mMazeGenerator = new RandomTreeMazeGenerator (Rows, Columns);
			break;
		case MazeGenerationAlgorithm.OldestTree:
			mMazeGenerator = new OldestTreeMazeGenerator (Rows, Columns);
			break;
		case MazeGenerationAlgorithm.RecursiveDivision:
			mMazeGenerator = new DivisionMazeGenerator (Rows, Columns);
			break;
		}

		mMazeGenerator.GenerateMaze();

        // Generate floors.
		for (int row = 0; row < Rows; row++) {
			for (int column = 0; column < Columns; column++) {
				float x = column*(CellWidth+(AddGaps?.2f:0));
				float z = row*(CellHeight+(AddGaps?.2f:0));
				GameObject tmp;
				tmp = Instantiate(Floor,new Vector3(x,0,z), Quaternion.Euler(0,0,0)) as GameObject;
				tmp.transform.parent = transform;
			}
		}

        // Generate pillars.
        if (Pillar != null) {
			for (int row = 0; row < Rows+1; row++) {
				for (int column = 0; column < Columns+1; column++) {
					float x = column*(CellWidth+(AddGaps?.2f:0));
					float z = row*(CellHeight+(AddGaps?.2f:0));
					GameObject tmp = Instantiate(Pillar,new Vector3(x-CellWidth/2,0,z-CellHeight/2),Quaternion.identity) as GameObject;
					tmp.transform.parent = transform;
				}
			}
		}

        // Generate outside walls.
        for (int row = 0; row < Rows; row++) {
            for (int column = 0; column < Columns; column++) {
                float x = column * (CellWidth + (AddGaps ? .2f : 0));
                float z = row * (CellHeight + (AddGaps ? .2f : 0));
                GameObject tmp;
                if (row == 0) {
                    tmp = Instantiate(Wall, new Vector3(x, 0, z - CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
                    tmp.transform.parent = transform;
                }
                else if (row == Rows - 1) {
                    tmp = Instantiate(Wall, new Vector3(x, 0, z + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
                    tmp.transform.parent = transform;
                }
                if (column == 0) {
                    tmp = Instantiate(Wall, new Vector3(x - CellWidth / 2, 0, z) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
                    tmp.transform.parent = transform;
                }
                else if (column == Columns - 1) {
                    tmp = Instantiate(Wall, new Vector3(x + CellWidth / 2, 0, z) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
                    tmp.transform.parent = transform;
                }
            }
        }

        // Generate inside walls.
        GameObject tmpWall;
        // Vertical
        // Column 1
        tmpWall = Instantiate(Wall, new Vector3(2, 0, -2 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(2, 0, 2 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(2, 0, 14 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(2, 0, 22 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(2, 0, 30 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(2, 0, 34 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 2
        tmpWall = Instantiate(Wall, new Vector3(6, 0, 2 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(6, 0, 6 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(6, 0, 14 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(6, 0, 26 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 3
        tmpWall = Instantiate(Wall, new Vector3(10, 0, 2 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(10, 0, 10 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(10, 0, 14 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(10, 0, 22 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(10, 0, 34 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 4
        tmpWall = Instantiate(Wall, new Vector3(14, 0, -2 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(14, 0, 2 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(14, 0, 14 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(14, 0, 18 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(14, 0, 30 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(14, 0, 34 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 5
        tmpWall = Instantiate(Wall, new Vector3(18, 0, 2 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(18, 0, 6 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(18, 0, 14 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 6
        tmpWall = Instantiate(Wall, new Vector3(22, 0, 2 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(22, 0, 10 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(22, 0, 18 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(22, 0, 22 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(22, 0, 30 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 7
        tmpWall = Instantiate(Wall, new Vector3(26, 0, 18 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(26, 0, 34 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 8
        tmpWall = Instantiate(Wall, new Vector3(30, 0, -2 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(30, 0, 6 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(30, 0, 14 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(30, 0, 26 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 9
        tmpWall = Instantiate(Wall, new Vector3(34, 0, -2 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(34, 0, 10 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(34, 0, 22 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(34, 0, 26 + CellWidth / 2) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Horizontal
        // Column 1
        tmpWall = Instantiate(Wall, new Vector3(0, 0, 10) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 2
        tmpWall = Instantiate(Wall, new Vector3(4, 0, 10) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(4, 0, 18) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 3
        tmpWall = Instantiate(Wall, new Vector3(8, 0, 26) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(8, 0, 34) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 4
        tmpWall = Instantiate(Wall, new Vector3(12, 0, 2) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(12, 0, 14) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 5
        tmpWall = Instantiate(Wall, new Vector3(16, 0, 22) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(16, 0, 30) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 6
        tmpWall = Instantiate(Wall, new Vector3(20, 0, 6) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(20, 0, 14) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(20, 0, 18) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 7
        tmpWall = Instantiate(Wall, new Vector3(24, 0, 34) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 8
        tmpWall = Instantiate(Wall, new Vector3(28, 0, 2) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(28, 0, 10) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(28, 0, 22) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(28, 0, 30) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 9
        tmpWall = Instantiate(Wall, new Vector3(32, 0, 22) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Column 10
        tmpWall = Instantiate(Wall, new Vector3(36, 0, 6) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(36, 0, 14) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(36, 0, 18) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        tmpWall = Instantiate(Wall, new Vector3(36, 0, 30) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        tmpWall.transform.parent = transform;

        // Generate kryptonites.
        GameObject tmpKryptonite;
        tmpKryptonite = Instantiate(Kryptonite, new Vector3(0, 1, 8), Quaternion.Euler(-90, 0, 0)) as GameObject;
        tmpKryptonite.transform.parent = transform;

        tmpKryptonite = Instantiate(Kryptonite, new Vector3(0, 1, 16), Quaternion.Euler(-90, 0, 0)) as GameObject;
        tmpKryptonite.transform.parent = transform;

        tmpKryptonite = Instantiate(Kryptonite, new Vector3(8, 1, 28), Quaternion.Euler(-90, 0, 0)) as GameObject;
        tmpKryptonite.transform.parent = transform;

        tmpKryptonite = Instantiate(Kryptonite, new Vector3(16, 1, 36), Quaternion.Euler(-90, 0, 0)) as GameObject;
        tmpKryptonite.transform.parent = transform;

        tmpKryptonite = Instantiate(Kryptonite, new Vector3(24, 1, 0), Quaternion.Euler(-90, 0, 0)) as GameObject;
        tmpKryptonite.transform.parent = transform;

        tmpKryptonite = Instantiate(Kryptonite, new Vector3(32, 1, 12), Quaternion.Euler(-90, 0, 0)) as GameObject;
        tmpKryptonite.transform.parent = transform;
    }
}
