﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeScript : MonoBehaviour
{
    public NodeScript[] neighbors;

    // Use this for initialization
    void Start()
    {

    }

    public int whatAmIToYou(NodeScript me)
    {
        return Array.IndexOf(neighbors, me);
    }

    void Update() 
    {
        DrawLines();
    }

    void DrawLines()
    {
        for (int i = 0; i < neighbors.Length - 1; i++)
        {
            if (neighbors[i])
            {
                Debug.DrawLine(transform.position, neighbors[i].transform.position, Color.blue);
            }
        }
    }

}
