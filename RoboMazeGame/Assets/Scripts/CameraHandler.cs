﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class CameraHandler : MonoBehaviour {

    public GameObject camera;
    public GameObject followCamera;
    public GameObject robot;
    public GameObject robotParent;
    public GameObject alien;
    public GameObject alienParent;

    private FollowCamera scriptCamera;
    private Robot scriptRobot;

    private bool startedLevel;
    private float timeLeft;

    public Text countdownText;
    public Text scoreText;

    void Start () {
        // Create maze view from above
        camera = (GameObject)Instantiate(camera, new Vector3(17.8f, 70, 17.2f), Quaternion.FromToRotation(new Vector3(0, 0, 0), new Vector3(0, 0, 0)));
        camera.transform.Rotate(new Vector3(90, 0, 0));

        // Spawning robot
        robotParent = new GameObject();
        robotParent.name = "RobotParent";
        robotParent.transform.position = new Vector3(0,1,0);

        robot = (GameObject)Instantiate(robot);
        robot.transform.parent = robotParent.transform;

        robotParent.transform.localScale = new Vector3(0.1F, 0.1F, 0.1F);

        // Set init values
        scriptRobot = (Robot)robot.GetComponent("Robot");
        scriptRobot.speed = 0;

        startedLevel = false;
        timeLeft = 30;
    }
    
    void Update () {
        // If still in above view
        if (!startedLevel)
        {
            // If touch
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                // Destroy camera
                Destroy(camera);

                // Create new camera to follow robot
                followCamera = (GameObject)Instantiate(followCamera);
                scriptCamera = (FollowCamera)followCamera.GetComponent("FollowCamera");
                scriptCamera.target = robot;

                // Update variable to started level state
                scriptRobot.speed = 4;
                startedLevel = true;
                StartCoroutine("LoseTime");
                Time.timeScale = 1;

                // Spawning alien
                alienParent = new GameObject();
                alienParent.name = "AlienParent";
                alienParent.transform.position = new Vector3(-4, -0.5F, 0);

                alien = (GameObject)Instantiate(alien);
                alien.transform.parent = alienParent.transform;

                alienParent.transform.localScale = new Vector3(4F, 4F, 4F);
                alien.transform.Rotate(new Vector3(0, -90, 0));
            }
        }
        else
        {
            // Updating countdown text
            countdownText.text = "Time Left: " + Mathf.Round(timeLeft);

            // If time is up
            if (timeLeft < 1)
            {
                AndroidNativeFunctions.ShowToast("Game Over", true);
                scriptRobot.speed = 0;
                countdownText.text = "Time is up!";
            }
        }
    }
    //Simple Coroutine
    IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            timeLeft--;
        }
    }
}
