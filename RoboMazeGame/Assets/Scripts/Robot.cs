﻿using UnityEngine;
using UnityEngine.UI;

public class Robot : MonoBehaviour
{
    public float SWIPE_THRESHOLD = 20;
    public float speed = 0;

    public AudioClip HitSound = null;
    private AudioSource mAudioSource = null;

    private GameObject robotParent;

    private int score;
    private Text scoreText;

    void Start()
    {
        mAudioSource = GetComponent<AudioSource>();
        robotParent = GameObject.Find("RobotParent");
        scoreText = GameObject.Find("Score").GetComponent<Text>();
        score = 0;
        scoreText.text = "Score: " + score.ToString();
    }

    void Update()
    {
        // Moving robot forward
        robotParent.transform.Translate(Vector3.forward * Time.deltaTime * speed);
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            // Get movement of the finger since last frame
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

            // Move object across XY plane
            // Right Swipe
            if (touchDeltaPosition.x > SWIPE_THRESHOLD)
            {
                transform.Rotate(0, 90, 0);
                robotParent.transform.Rotate(new Vector3(0, 90, 0));
            }
            // Left Swipe
            else if (touchDeltaPosition.x < -SWIPE_THRESHOLD)
            {
                transform.Rotate(0, -90, 0);
                robotParent.transform.Rotate(new Vector3(0, -90, 0));
            }
        }
    }

    // Collision detection
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Kryptonite"))
        {            
            Destroy(other.gameObject);
            score += 10;
            scoreText.text = "Score: " + score.ToString();
        }
        else if (other.gameObject.tag.Equals("Wall"))
        {
            speed = 0;
            AndroidNativeFunctions.ShowToast("Game Over", true);
        }
    }
}