﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pathPlayerScript : MonoBehaviour
{
    public float MoveSpeed;
    private NodeScript NodeToVisit;
    private NodeScript lastVisitedNode;
    private bool directionProcessed;
    private int nextTurnAsIndex;
    public KeyCode turnRight = KeyCode.D;
    public KeyCode turnLeft = KeyCode.A;
    private Quaternion diseredRot;


    // Use this for initialization
    void Start()
    {
        //Initalize run
        nextTurnAsIndex = 2;
        GameObject NodeScriptGameObejct = GameObject.FindWithTag("startPosition");
        if (NodeScriptGameObejct != null)
        {
            lastVisitedNode = NodeScriptGameObejct.GetComponent<NodeScript>();
            NodeToVisit = lastVisitedNode.neighbors[0];
            Vector3 newDirectionVector =
                (NodeToVisit.transform.position - lastVisitedNode.transform.position).normalized;
            diseredRot = Quaternion.LookRotation(newDirectionVector);
            nextTurnAsIndex = 2;
            directionProcessed = true;
        }
        else
        {
            Debug.Log("Start position not found");
            throw new System.Exception("start position not found");
        }
    }

    // Update is called once per frame
    void Update()
    {

        //Do rotation
        transform.rotation = Quaternion.Lerp(
            transform.rotation,
            diseredRot,
            5 * Time.deltaTime);

        //Move to node
        transform.position = Vector3.MoveTowards(transform.position, NodeToVisit.transform.position, MoveSpeed * Time.deltaTime);

        //Keep track of user input
        if (1.5 > Vector3.Distance(transform.position, NodeToVisit.transform.position))
        {
            trackUserInput();
        }
        //Debug.Log("Distance: " + Vector3.Distance(body.transform.position, NodeToVisit.transform.position));
        if (0.01 > Vector3.Distance(transform.position, NodeToVisit.transform.position) && !directionProcessed)
        { //At postion
            processDirention();
        }

        //Make sure turn is only processed once
        if (0.02 < Vector3.Distance(transform.position, NodeToVisit.transform.position))
        {
            directionProcessed = false;
        }
    }

    void trackUserInput()
    {
        //Process input
        if (Input.GetKeyDown(turnRight)) //turndown relative right
        {
            nextTurnAsIndex = -1;

        }
        else if (Input.GetKeyDown(turnLeft)) //Turn relative left
        {
            nextTurnAsIndex = 1;
        }
    }

    void processDirention()
    {

        int entranceIndex = NodeToVisit.whatAmIToYou(lastVisitedNode);


        int indexOnNextnodeToVisit = modulu((entranceIndex + nextTurnAsIndex), NodeToVisit.neighbors.Length);
        Debug.Log("Index: " + indexOnNextnodeToVisit + " = " + (entranceIndex + nextTurnAsIndex) + " % " + NodeToVisit.neighbors.Length);

        NodeScript tempNodeToVisit = NodeToVisit.neighbors[indexOnNextnodeToVisit];

        if (tempNodeToVisit != null)
        {
            lastVisitedNode = NodeToVisit;
            NodeToVisit = tempNodeToVisit;
            Vector3 newDirectionVector =
                (NodeToVisit.transform.position - lastVisitedNode.transform.position).normalized;
            diseredRot = Quaternion.LookRotation(newDirectionVector);
            nextTurnAsIndex = 2;
            directionProcessed = true;
        }
        else
        {
            Debug.Log("You cannot go in this direction => DEAD");
        }
    }

    private int modulu(int x, int m)
    {
        return (x % m + m) % m;
    }
}

