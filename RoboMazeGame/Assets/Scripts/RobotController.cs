﻿using UnityEngine;

public class RobotController : MonoBehaviour {
    private GameObject robot;
    private GameObject robotParent;

    // Use this for initialization
    void Start () {
        robot = GameObject.Find("Robot");
        robotParent = new GameObject();
        robot.transform.parent = robotParent.transform;
        robotParent.transform.position = (new Vector3(0, -5, 0));
        robotParent.transform.Rotate(new Vector3(20, 160, 0));
        robotParent.transform.localScale = (new Vector3(2f, 2f, 2f));
    }
	
	// Update is called once per frame
	void Update () {
        
    }
}
